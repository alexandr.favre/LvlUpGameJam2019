﻿using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    // Public attributes
    public Canvas canvasShop;
    public Canvas canvasWin;
    public Menu_scene menuScene;
    public Inventary inventary;
    public Rigidbody2D rb2dPlayer;
    public Text nbGreenTxt;
    public Text nbBlueTxt;
    public Text nbRedTxt;
    public Text nbYellowTxt;
    public Text nbRYellowTxt;
    public Text nbROverkitchTxt;
    public Text nbRBlueTxt;
    public Text nbTRYellowTxt;
    public Text nbTRBlueTxt;
    public Text nbRGreenTxt;
    public Text nbFinalTxt;
    public Text nbRRedTxt;
    public Text nbTRPinkTxt;
    public Text nbTROrangeTxt;
    public Text nbCashTxt;
    public Text nbRBeeTxt;
    public Text estimatedAmount;

    // Prices
    public int greenFishPrice;
    public int blueFishPrice;
    public int redFishPrice;
    public int yellowFishPrice;
    public int rYellowFishPrice;
    public int rOverkitchFishPrice;
    public int rBlueFishPrice;
    public int tRYellowFishPrice;
    public int tRBlueFishPrice;
    public int rGreenFishPrice;
    public int finalFishPrice;
    public int rRedFishPrice;
    public int tRPinkFishPrice;
    public int tROrangeFishPrice;
    public int rBeeFishPrice;

    // Selected fish
    public Text nbGreenFishSelected;
    public Text nbBlueFishSelected;
    public Text nbRedFishSelected;
    public Text nbYellowFishSelected;
    public Text nbRYellowFishSelected;
    public Text nbROverkitchFishSelected;
    public Text nbRBlueFishSelected;
    public Text nbTRYellowFishSelected;
    public Text nbTRBlueFishSelected;
    public Text nbRGreenFishSelected;
    public Text nbFinalFishSelected;
    public Text nbRRedFishSelected;
    public Text nbTRPinkFishSelected;
    public Text nbTROrangeFishSelected;
    public Text nbRBeeFishSelected;

    int nbGreenFishSelectedInt;
    int nbBlueFishSelectedInt;
    int nbRedFishSelectedInt;
    int nbYellowFishSelectedInt;
    int nbRYellowFishSelectedInt;
    int nbROverkitchFishSelectedInt;
    int nbRBlueFishSelectedInt;
    int nbTRYellowFishSelectedInt;
    int nbTRBlueFishSelectedInt;
    int nbRGreenFishSelectedInt;
    int nbFinalFishSelectedInt;
    int nbRRedFishSelectedInt;
    int nbTRPinkFishSelectedInt;
    int nbTROrangeFishSelectedInt;
    int nbRBeeFishSelectedInt;

    private bool gameWin = false;
    private bool isOpen = false;
    public Collider2D playerCol;
    int sum = 0;

    private void Update()
    {
        if (isOpen && Input.GetButtonDown("Cancel") && !gameWin)
        {
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezeRotation;
            canvasShop.enabled = false;
            isOpen = false;
            inventary.isAvailable = true;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isOpen = true;
            inventary.isAvailable = false;
            resetToZero();
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezePosition;

            nbGreenTxt.text = inventary.nbGreenFish.ToString();
            nbBlueTxt.text = inventary.nbBlueFish.ToString();
            nbRedTxt.text = inventary.nbRedFish.ToString();
            nbYellowTxt.text = inventary.nbYellowFish.ToString();
            nbRYellowTxt.text = inventary.nbRYellowFish.ToString();
            nbROverkitchTxt.text = inventary.nbROverkitchFish.ToString();
            nbRBlueTxt.text = inventary.nbRBlueFish.ToString();
            nbTRYellowTxt.text = inventary.nbTRYellowFish.ToString();
            nbTRBlueTxt.text = inventary.nbTRBlueFish.ToString();
            nbRGreenTxt.text = inventary.nbRGreenFish.ToString();
            nbFinalTxt.text = inventary.nbFinalFish.ToString();
            nbRRedTxt.text = inventary.nbRRedFish.ToString();
            nbTRPinkTxt.text = inventary.nbTRPinkFish.ToString();
            nbTROrangeTxt.text = inventary.nbTROrangeFish.ToString();
            nbCashTxt.text = inventary.cash.ToString();
            nbRBeeTxt.text = inventary.nbRBeeFish.ToString();

            canvasShop.enabled = true;
        }
    }

    public void sell()
    {
        if (isOpen)
        {
            nbGreenFishSelectedInt = int.Parse(nbGreenFishSelected.text);
            nbBlueFishSelectedInt = int.Parse(nbBlueFishSelected.text);
            nbRedFishSelectedInt = int.Parse(nbRedFishSelected.text);
            nbYellowFishSelectedInt = int.Parse(nbYellowFishSelected.text);
            nbRYellowFishSelectedInt = int.Parse(nbRYellowFishSelected.text);
            nbROverkitchFishSelectedInt = int.Parse(nbROverkitchFishSelected.text);
            nbRBlueFishSelectedInt = int.Parse(nbRBlueFishSelected.text);
            nbTRYellowFishSelectedInt = int.Parse(nbTRYellowFishSelected.text);
            nbTRBlueFishSelectedInt = int.Parse(nbTRBlueFishSelected.text);
            nbRGreenFishSelectedInt = int.Parse(nbRGreenFishSelected.text);
            nbFinalFishSelectedInt = int.Parse(nbFinalFishSelected.text);
            nbRRedFishSelectedInt = int.Parse(nbRRedFishSelected.text);
            nbTRPinkFishSelectedInt = int.Parse(nbTRPinkFishSelected.text);
            nbTROrangeFishSelectedInt = int.Parse(nbTROrangeFishSelected.text);
            nbRBeeFishSelectedInt = int.Parse(nbRBeeFishSelected.text);

            calcSum();

            // Remove items from user
            removeItems();

            // Add money to the user
            inventary.cash += sum;

            if (inventary.cash >= 1000000)
            {
                gameWin = true;
                menuScene.isAvailable = false;
                menuScene.isOpen = true;
                canvasWin.enabled = true;
            }

            nbCashTxt.text = inventary.cash.ToString();
            resetToZero();
        }
    }

    void calcSum()
    {
        sum = greenFishPrice * nbGreenFishSelectedInt
        + blueFishPrice * nbBlueFishSelectedInt
        + redFishPrice * nbRedFishSelectedInt
        + yellowFishPrice * nbYellowFishSelectedInt
        + rYellowFishPrice * nbRYellowFishSelectedInt
        + rOverkitchFishPrice * nbROverkitchFishSelectedInt
        + rBlueFishPrice * nbRBlueFishSelectedInt
        + tRYellowFishPrice * nbTRYellowFishSelectedInt
        + tRBlueFishPrice * nbTRBlueFishSelectedInt
        + rGreenFishPrice * nbRGreenFishSelectedInt
        + finalFishPrice * nbFinalFishSelectedInt
        + rRedFishPrice * nbRRedFishSelectedInt
        + tRPinkFishPrice * nbTRPinkFishSelectedInt
        + tROrangeFishPrice * nbTROrangeFishSelectedInt
        + rBeeFishPrice * nbRBeeFishSelectedInt;
    }

    void onUpdate()
    {
        calcSum();
        estimatedAmount.text = "" + sum;
    }

    void removeItems()
    {
        inventary.nbGreenFish = int.Parse(nbGreenTxt.text);
        inventary.nbBlueFish = int.Parse(nbBlueTxt.text);
        inventary.nbRedFish = int.Parse(nbRedTxt.text);
        inventary.nbYellowFish = int.Parse(nbYellowTxt.text);
        inventary.nbRYellowFish = int.Parse(nbRYellowTxt.text);
        inventary.nbROverkitchFish = int.Parse(nbROverkitchTxt.text);
        inventary.nbRBlueFish = int.Parse(nbRBlueTxt.text);
        inventary.nbTRYellowFish = int.Parse(nbTRYellowTxt.text);
        inventary.nbTRBlueFish = int.Parse(nbTRBlueTxt.text);
        inventary.nbRGreenFish = int.Parse(nbRGreenTxt.text);
        inventary.nbFinalFish = int.Parse(nbFinalTxt.text);
        inventary.nbRRedFish = int.Parse(nbRRedTxt.text);
        inventary.nbTRPinkFish = int.Parse(nbTRPinkTxt.text);
        inventary.nbTROrangeFish = int.Parse(nbTROrangeTxt.text);
        inventary.nbRBeeFish = int.Parse(nbRBeeTxt.text);
    }

    void resetToZero()
    {
        nbGreenTxt.text = inventary.nbGreenFish.ToString();
        nbBlueTxt.text = inventary.nbBlueFish.ToString();
        nbRedTxt.text = inventary.nbRedFish.ToString();
        nbYellowTxt.text = inventary.nbYellowFish.ToString();
        nbRYellowTxt.text = inventary.nbRYellowFish.ToString();
        nbROverkitchTxt.text = inventary.nbROverkitchFish.ToString();
        nbRBlueTxt.text = inventary.nbRBlueFish.ToString();
        nbTRYellowTxt.text = inventary.nbTRYellowFish.ToString();
        nbTRBlueTxt.text = inventary.nbTRBlueFish.ToString();
        nbRGreenTxt.text = inventary.nbRGreenFish.ToString();
        nbFinalTxt.text = inventary.nbFinalFish.ToString();
        nbRRedTxt.text = inventary.nbRRedFish.ToString();
        nbTRPinkTxt.text = inventary.nbTRPinkFish.ToString();
        nbTROrangeTxt.text = inventary.nbTROrangeFish.ToString();
        nbRBeeTxt.text = inventary.nbRBeeFish.ToString();

        nbGreenFishSelectedInt = 0;
        nbBlueFishSelectedInt = 0;
        nbRedFishSelectedInt = 0;
        nbYellowFishSelectedInt = 0;
        nbRYellowFishSelectedInt = 0;
        nbROverkitchFishSelectedInt = 0;
        nbRBlueFishSelectedInt = 0;
        nbTRYellowFishSelectedInt = 0;
        nbTRBlueFishSelectedInt = 0;
        nbRGreenFishSelectedInt = 0;
        nbFinalFishSelectedInt = 0;
        nbRRedFishSelectedInt = 0;
        nbTRPinkFishSelectedInt = 0;
        nbTROrangeFishSelectedInt = 0;
        nbRBeeFishSelectedInt = 0;

        nbGreenFishSelected.text = "0";
        nbBlueFishSelected.text = "0";
        nbRedFishSelected.text = "0";
        nbYellowFishSelected.text = "0";
        nbRYellowFishSelected.text = "0";
        nbROverkitchFishSelected.text = "0";
        nbRBlueFishSelected.text = "0";
        nbTRYellowFishSelected.text = "0";
        nbTRBlueFishSelected.text = "0";
        nbRGreenFishSelected.text = "0";
        nbFinalFishSelected.text = "0";
        nbRRedFishSelected.text = "0";
        nbTRPinkFishSelected.text = "0";
        nbTROrangeFishSelected.text = "0";
        nbRBeeFishSelected.text = "0";

        estimatedAmount.text = "0";
    }

    // Green fish
    public void addGreenSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbGreenTxt.text);
            if (nbPossede > 0)
            {
                nbGreenFishSelected.text = (++nbGreenFishSelectedInt).ToString();
                nbGreenTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeGreenSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbGreenTxt.text);
            if (nbGreenFishSelectedInt > 0)
            {
                nbGreenFishSelected.text = (--nbGreenFishSelectedInt).ToString();
                nbGreenTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Blue fish
    public void addBlueSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbBlueTxt.text);
            if (nbPossede > 0)
            {
                nbBlueFishSelected.text = (++nbBlueFishSelectedInt).ToString();
                nbBlueTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeBlueSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbBlueTxt.text);
            if (nbBlueFishSelectedInt > 0)
            {
                nbBlueFishSelected.text = (--nbBlueFishSelectedInt).ToString();
                nbBlueTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Red fish
    public void addRedSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRedTxt.text);
            if (nbPossede > 0)
            {
                nbRedFishSelected.text = (++nbRedFishSelectedInt).ToString();
                nbRedTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeRedSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRedTxt.text);
            if (nbRedFishSelectedInt > 0)
            {
                nbRedFishSelected.text = (--nbRedFishSelectedInt).ToString();
                nbRedTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Yellow fish
    public void addYellowSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbYellowTxt.text);
            if (nbPossede > 0)
            {
                nbYellowFishSelected.text = (++nbYellowFishSelectedInt).ToString();
                nbYellowTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeYellowSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbYellowTxt.text);
            if (nbYellowFishSelectedInt > 0)
            {
                nbYellowFishSelected.text = (--nbYellowFishSelectedInt).ToString();
                nbYellowTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Bee fish
    public void addRBeeSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRBeeTxt.text);
            if (nbPossede > 0)
            {
                nbRBeeFishSelected.text = (++nbRBeeFishSelectedInt).ToString();
                nbRBeeTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeRBeeSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRBeeTxt.text);
            if (nbRBeeFishSelectedInt > 0)
            {
                nbRBeeFishSelected.text = (--nbRBeeFishSelectedInt).ToString();
                nbRBeeTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Rare blue fish
    public void addRareBlueSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRBlueTxt.text);
            if (nbPossede > 0)
            {
                nbRBlueFishSelected.text = (++nbRBlueFishSelectedInt).ToString();
                nbRBlueTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeRareBlueSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRBlueTxt.text);
            if (nbRBlueFishSelectedInt > 0)
            {
                nbRBlueFishSelected.text = (--nbRBlueFishSelectedInt).ToString();
                nbRBlueTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Overkitch fish
    public void addROverkitchSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbROverkitchTxt.text);
            if (nbPossede > 0)
            {
                nbROverkitchFishSelected.text = (++nbROverkitchFishSelectedInt).ToString();
                nbROverkitchTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeROverkitchSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbROverkitchTxt.text);
            if (nbROverkitchFishSelectedInt > 0)
            {
                nbROverkitchFishSelected.text = (--nbROverkitchFishSelectedInt).ToString();
                nbROverkitchTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Rare yellow fish
    public void addRYellowSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRYellowTxt.text);
            if (nbPossede > 0)
            {
                nbRYellowFishSelected.text = (++nbRYellowFishSelectedInt).ToString();
                nbRYellowTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeRYellowSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRYellowTxt.text);
            if (nbRYellowFishSelectedInt > 0)
            {
                nbRYellowFishSelected.text = (--nbRYellowFishSelectedInt).ToString();
                nbRYellowTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Rare red fish
    public void addRRedSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRRedTxt.text);
            if (nbPossede > 0)
            {
                nbRRedFishSelected.text = (++nbRRedFishSelectedInt).ToString();
                nbRRedTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeRRedSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRRedTxt.text);
            if (nbRRedFishSelectedInt > 0)
            {
                nbRRedFishSelected.text = (--nbRRedFishSelectedInt).ToString();
                nbRRedTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Rare green fish
    public void addRGreenSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRGreenTxt.text);
            if (nbPossede > 0)
            {
                nbRGreenFishSelected.text = (++nbRGreenFishSelectedInt).ToString();
                nbRGreenTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeRGreenSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbRGreenTxt.text);
            if (nbRGreenFishSelectedInt > 0)
            {
                nbRGreenFishSelected.text = (--nbRGreenFishSelectedInt).ToString();
                nbRGreenTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // Super rare blue fish
    public void addTRBlueSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTRBlueTxt.text);
            if (nbPossede > 0)
            {
                nbTRBlueFishSelected.text = (++nbTRBlueFishSelectedInt).ToString();
                nbTRBlueTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeTRBlueSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTRBlueTxt.text);
            if (nbTRBlueFishSelectedInt > 0)
            {
                nbTRBlueFishSelected.text = (--nbTRBlueFishSelectedInt).ToString();
                nbTRBlueTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // super rare yellow fish
    public void addTRYellowSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTRYellowTxt.text);
            if (nbPossede > 0)
            {
                nbTRYellowFishSelected.text = (++nbTRYellowFishSelectedInt).ToString();
                nbTRYellowTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeTRYellowSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTRYellowTxt.text);
            if (nbTRYellowFishSelectedInt > 0)
            {
                nbTRYellowFishSelected.text = (--nbTRYellowFishSelectedInt).ToString();
                nbTRYellowTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // super rare orange fish
    public void addTROrangeSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTROrangeTxt.text);
            if (nbPossede > 0)
            {
                nbTROrangeFishSelected.text = (++nbTROrangeFishSelectedInt).ToString();
                nbTROrangeTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeTROrangeSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTROrangeTxt.text);
            if (nbTROrangeFishSelectedInt > 0)
            {
                nbTROrangeFishSelected.text = (--nbTROrangeFishSelectedInt).ToString();
                nbTROrangeTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // super rare pink fish
    public void addTRPinkSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTRPinkTxt.text);
            if (nbPossede > 0)
            {
                nbTRPinkFishSelected.text = (++nbTRPinkFishSelectedInt).ToString();
                nbTRPinkTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeTRPinkSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbTRPinkTxt.text);
            if (nbTRPinkFishSelectedInt > 0)
            {
                nbTRPinkFishSelected.text = (--nbTRPinkFishSelectedInt).ToString();
                nbTRPinkTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }

    // ultra rare fish
    public void addFinalSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbFinalTxt.text);
            if (nbPossede > 0)
            {
                nbFinalFishSelected.text = (++nbFinalFishSelectedInt).ToString();
                nbFinalTxt.text = (nbPossede - 1).ToString();
                onUpdate();
            }
        }
    }

    public void removeFinalSell()
    {
        if (isOpen)
        {
            int nbPossede = int.Parse(nbFinalTxt.text);
            if (nbFinalFishSelectedInt > 0)
            {
                nbFinalFishSelected.text = (--nbFinalFishSelectedInt).ToString();
                nbFinalTxt.text = (nbPossede + 1).ToString();
                onUpdate();
            }
        }
    }
}
