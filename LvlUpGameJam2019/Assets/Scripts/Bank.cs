﻿using UnityEngine;
using UnityEngine.UI;

public class Bank : MonoBehaviour
{

    // Imperative components
    public Canvas canvasBank;
    public Inventary inventary;
    public Rigidbody2D rb2dPlayer;

    // Components
    public Text playerAmmount;
    public Text accountAmmount;
    public Text max;
    public Slider slider;
    public InputField currentSelection;

    // private components
    private bool isOpen;

    public void Start()
    {
        slider.onValueChanged.AddListener(delegate { SliderValueChanged(); });
        currentSelection.onValueChanged.AddListener(delegate { InputValueChanged(); });
    }

    public void SliderValueChanged()
    {
        currentSelection.text = slider.value.ToString();
    }

    public void InputValueChanged()
    {
        if (!currentSelection.text.Contains("-"))
        {
            if (int.Parse(currentSelection.text) > int.Parse(max.text))
            {
                currentSelection.text = max.text;
            }
        }
        else
        {
            currentSelection.text = "0";
        }

        slider.value = float.Parse(currentSelection.text);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isOpen = true;
            inventary.isAvailable = false;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezePosition;

            Init();

            canvasBank.enabled = true;
        }
    }

    private void Update()
    {
        if (isOpen && Input.GetButtonDown("Cancel"))
        {
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezeRotation;
            canvasBank.enabled = false;
            isOpen = false;
            inventary.isAvailable = true;
        }
    }

    public void Withdraw()
    {
        int current = int.Parse(currentSelection.text);
        int account = int.Parse(accountAmmount.text);
        if (current <= account)
        {
            inventary.cash += current;
            account -= current;
            accountAmmount.text = account.ToString();
            Init();
        }
    }

    public void Deposit()
    {
        int current = int.Parse(currentSelection.text);
        if (current <= inventary.cash)
        {
            int account = int.Parse(accountAmmount.text);
            inventary.cash -= current;
            account += current;
            accountAmmount.text = account.ToString();
            Init();
        }
    }

    void Init()
    {
        int maximum = 0;
        if (int.Parse(accountAmmount.text) > inventary.cash)
        {
            maximum = int.Parse(accountAmmount.text);
        }
        else
        {
            maximum = inventary.cash;
        }

        max.text = maximum.ToString();
        slider.value = 0;
        slider.maxValue = maximum;
        slider.wholeNumbers = true;
        playerAmmount.text = inventary.cash.ToString();
    }
}