﻿using UnityEngine;

public class Welcome : MonoBehaviour
{

    public Inventary inventary;
    public Canvas welcomeCanvas;
    public Rigidbody2D rb2dPlayer;

    private bool isOpen = true;

    void Start()
    {
        inventary.isAvailable = false;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel") && isOpen)
        {
            isOpen = false;
            inventary.isAvailable = true;
            welcomeCanvas.enabled = false;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }
}
