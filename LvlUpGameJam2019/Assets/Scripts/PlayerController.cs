﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Inventary inventary;
    public Image lifeBar;
    public Image oxygenBar;
    public Animator weaponAnimator;
    public GameObject deadShip;
    public SpriteRenderer water;

    public float bouncingLimitLowHigh = 5f;
    public float bouncingHigh = 2f;
    public float bouncingLow = 0.5f;
    public float speed = 40f;
    public float velocityMax = 10f;
    public Vector2 initialPos = new Vector2(0, 0.1f);

    public GameObject greenLight;
    public GameObject blueLight;
    public GameObject redLight;
    public GameObject yellowLight;

    public float oxygenMaxQty = 100f;
    public float oxygenQty = 100f;

    public float lifeMaxQty = 100f;
    public float lifeQty = 100f;
    public float minYPressure = -30f;

    public int pressureLevel = 0;
    public int oxygenLevel = 0;

    private float oxygenRegenSpeed = 40f;

    private bool isUnderPressureLevel = false;
    private bool isDead = false;

    private bool isOverkitchViewable = false;
    private bool isTROrangeViewable = false;
    private bool isFinalFishViewable = false;

    private bool isGreenLightOn;
    private bool isBlueLightOn;
    private bool isRedLightOn;
    private bool isYellowLightOn;

    private Rigidbody2D rb2d;
    bool isFacingRight = true;
    bool isBouncing = false;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    GameObject[] FindObjectsByTag(string tag)
    {
        List<GameObject> validTransforms = new List<GameObject>();
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].gameObject.CompareTag(tag))
                {
                    validTransforms.Add(objs[i].gameObject);
                }
            }
        }
        return validTransforms.ToArray();
    }

    void Update()
    {
        if (transform.position.y < -1)
        {
            oxygenQty -= 8f * Time.deltaTime;
            oxygenBar.fillAmount = oxygenQty / oxygenMaxQty;
        }
        else if (transform.position.y > -1 && oxygenQty < oxygenMaxQty)
        {
            oxygenQty += oxygenRegenSpeed * Time.deltaTime;
            oxygenBar.fillAmount = oxygenQty / oxygenMaxQty;
        }

        if (transform.position.y < minYPressure)
        {
            if (!isUnderPressureLevel)
            {
                water.color = new Color(255f, 0f, 0f, 0.54f);
                isUnderPressureLevel = true;
            }
            lifeQty -= 16f * Time.deltaTime;
            lifeBar.fillAmount = lifeQty / lifeMaxQty;
        }
        else if (transform.position.y > minYPressure && isUnderPressureLevel)
        {
            water.color = new Color(255f, 0f, 0f, 0f);
            isUnderPressureLevel = false;
        }
        else if (transform.position.y > -1 && lifeQty < lifeMaxQty)
        {
            lifeQty += 40f * Time.deltaTime;
            lifeBar.fillAmount = lifeQty / lifeMaxQty;
        }

        if ((oxygenQty <= 0 || lifeQty <= 0) && !isDead)
        {
            Die();
        }

        if (Input.GetButtonDown("Jump") && !isDead)
        {
            weaponAnimator.SetBool("DoAction", true);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && !isDead)
        {
            if (inventary.nbGreenBall > 0 && !isGreenLightOn)
            {
                greenLight.SetActive(true);
                isGreenLightOn = true;
            }
            else DisableGreenLight();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && !isDead)
        {
            if (inventary.nbBlueBall > 0 && !isBlueLightOn)
            {
                blueLight.SetActive(true);
                isBlueLightOn = true;
            }
            else DisableBlueLight();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) && !isDead)
        {
            if (inventary.nbRedBall > 0 && !isRedLightOn)
            {
                redLight.SetActive(true);
                isRedLightOn = true;
            }
            else DisableRedLight();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4) && !isDead)
        {
            if (inventary.nbYellowBall > 0 && !isYellowLightOn)
            {
                yellowLight.SetActive(true);
                isYellowLightOn = true;
            }
            else DisableYellowLight();
        }

        if (isGreenLightOn && isBlueLightOn && !isOverkitchViewable)
        {
            GameObject[] gameObjectArray = FindObjectsByTag("ROverkitchFish");
            foreach (GameObject go in gameObjectArray)
            {
                go.SetActive(true);
            }
            isOverkitchViewable = true;
        }
        else if ((!isGreenLightOn || !isBlueLightOn) && isOverkitchViewable)
        {
            GameObject[] gameObjectArray = FindObjectsByTag("ROverkitchFish");
            foreach (GameObject go in gameObjectArray)
            {
                go.SetActive(false);
            }
            isOverkitchViewable = false;
        }

        if (isRedLightOn && isYellowLightOn && !isTROrangeViewable)
        {
            GameObject[] gameObjectArray = FindObjectsByTag("TROrangeFish");
            foreach (GameObject go in gameObjectArray)
            {
                go.SetActive(true);
            }
            isTROrangeViewable = true;
        }
        else if ((!isRedLightOn || !isYellowLightOn) && isTROrangeViewable)
        {
            GameObject[] gameObjectArray = FindObjectsByTag("TROrangeFish");
            foreach (GameObject go in gameObjectArray)
            {
                go.SetActive(false);
            }
            isTROrangeViewable = false;
        }

        if (isRedLightOn && isYellowLightOn && isGreenLightOn && isBlueLightOn && !isFinalFishViewable)
        {
            GameObject[] gameObjectArray = FindObjectsByTag("FinalFish");
            foreach (GameObject go in gameObjectArray)
            {
                go.SetActive(true);
            }
            isFinalFishViewable = true;
        }
        else if ((!isRedLightOn || !isYellowLightOn || !isGreenLightOn || !isBlueLightOn) && isFinalFishViewable)
        {
            GameObject[] gameObjectArray = FindObjectsByTag("FinalFish");
            foreach (GameObject go in gameObjectArray)
            {
                go.SetActive(false);
            }
            isFinalFishViewable = false;
        }
    }

    void FixedUpdate()
    {
        if (rb2d.velocity.y <= -velocityMax)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, -velocityMax);
        }

        if (!isDead)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            if (moveVertical < 0) moveVertical = 0;

            if (rb2d.velocity.x <= -velocityMax)
            {
                if (moveHorizontal < 0) moveHorizontal = 0;
            }
            else if (rb2d.velocity.x >= velocityMax)
            {
                if (moveHorizontal > 0) moveHorizontal = 0;
            }

            if (rb2d.velocity.y <= -velocityMax)
            {
                if (moveVertical < 0) moveVertical = 0;
            }
            else if (rb2d.velocity.y >= velocityMax)
            {
                moveVertical = 0;
            }

            if (transform.position.y >= 0 && !isBouncing)
            {
                isBouncing = true;
                moveVertical = 0;
                if (rb2d.velocity.y > bouncingLimitLowHigh) rb2d.velocity = new Vector2(rb2d.velocity.x, bouncingHigh);
                else rb2d.velocity = new Vector2(rb2d.velocity.x, bouncingLow);
            }
            else if (transform.position.y >= 0 && isBouncing)
            {
                moveVertical = 0;
            }
            else if (transform.position.y <= 0 && isBouncing)
            {
                isBouncing = false;
            }

            Vector2 movement = new Vector2(moveHorizontal * speed / 2, moveVertical * speed);

            rb2d.AddForce(movement);

            if ((moveHorizontal > 0 && !isFacingRight) || (moveHorizontal < 0 && isFacingRight))
            {
                Flip();
            }
        }
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Die()
    {
        isDead = true;
        deadShip.SetActive(true);

        inventary.cash = 0;
        inventary.nbBlueFish = 0;
        inventary.nbFinalFish = 0;
        inventary.nbGreenFish = 0;
        inventary.nbRBeeFish = 0;
        inventary.nbRBlueFish = 0;
        inventary.nbRedFish = 0;
        inventary.nbRGreenFish = 0;
        inventary.nbROverkitchFish = 0;
        inventary.nbRRedFish = 0;
        inventary.nbRYellowFish = 0;
        inventary.nbTRBlueFish = 0;
        inventary.nbTROrangeFish = 0;
        inventary.nbTRPinkFish = 0;
        inventary.nbTRYellowFish = 0;
        inventary.nbYellowFish = 0;

        inventary.nbBlueBall = 0;
        inventary.nbGreenBall = 0;
        inventary.nbRedBall = 0;
        inventary.nbYellowBall = 0;

        StartCoroutine(waiter());
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(2);

        transform.position = initialPos;
        deadShip.SetActive(false);
        oxygenBar.fillAmount = 1;
        lifeBar.fillAmount = 1;
        oxygenQty = oxygenMaxQty;
        lifeQty = lifeMaxQty;
        rb2d.velocity = new Vector2(0, 0);

        DisableBlueLight();
        DisableGreenLight();
        DisableRedLight();
        DisableYellowLight();

        isDead = false;
    }

    public void UpgradePressureAmmoLvl(int toLvl)
    {
        pressureLevel = toLvl;
        if (pressureLevel == 3) minYPressure = -130f;
        else if (pressureLevel == 2) minYPressure = -90f;
        else if (pressureLevel == 1) minYPressure = -60f;
    }

    public void UpgradeOxygenCapLvl(int toLvl)
    {
        oxygenLevel = toLvl;
        if (oxygenLevel == 3)
        {
            oxygenMaxQty = 1000f;
            oxygenRegenSpeed = 200f;
        }
        else if (oxygenLevel == 2)
        {
            oxygenMaxQty = 500f;
            oxygenRegenSpeed = 120f;
        }
        else if (oxygenLevel == 1)
        {
            oxygenMaxQty = 300f;
            oxygenRegenSpeed = 80f;
        }
    }

    public void DisableGreenLight()
    {
        isGreenLightOn = false;
        greenLight.SetActive(false);
    }

    public void DisableBlueLight()
    {
        isBlueLightOn = false;
        blueLight.SetActive(false);
    }

    public void DisableRedLight()
    {
        isRedLightOn = false;
        redLight.SetActive(false);
    }

    public void DisableYellowLight()
    {
        isYellowLightOn = false;
        yellowLight.SetActive(false);
    }
}
