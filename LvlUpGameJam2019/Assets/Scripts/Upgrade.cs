﻿using UnityEngine;
using UnityEngine.UI;

public class Upgrade : MonoBehaviour
{
    public PlayerController playerController;
    public Canvas upgradeCanvas;
    public Inventary inventary;
    public Rigidbody2D rb2dPlayer;

    public GameObject pressureAmmoLvl1;
    public GameObject pressureAmmoLvl2;
    public GameObject pressureAmmoLvl3;

    public GameObject oxygenCapLvl1;
    public GameObject oxygenCapLvl2;
    public GameObject oxygenCapLvl3;

    public Button pressureAmmoLvl1Btn;
    public Button pressureAmmoLvl2Btn;
    public Button pressureAmmoLvl3Btn;

    public Button oxygenCapLvl1Btn;
    public Button oxygenCapLvl2Btn;
    public Button oxygenCapLvl3Btn;

    public Text cashTxt;

    private bool isOpen = false;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isOpen = true;
            inventary.isAvailable = false;
            upgradeCanvas.enabled = true;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezePosition;

            UpdateView();
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel") && isOpen)
        {
            isOpen = false;
            inventary.isAvailable = true;
            upgradeCanvas.enabled = false;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }

    void UpdateView()
    {
        cashTxt.text = inventary.cash.ToString();

        switch (playerController.pressureLevel)
        {
            case 0:
                if (inventary.cash < 5000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 5000 && inventary.cash < 20000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 20000 && inventary.cash < 50000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 50000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0f);
                }
                break;
            case 1:
                if (inventary.cash < 20000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 20000 && inventary.cash < 50000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 50000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0f);
                }
                break;
            case 2:
                if (inventary.cash < 20000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl2Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 50000)
                {
                    pressureAmmoLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl2Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    pressureAmmoLvl3Btn.image.color = new Color(255f, 0f, 0f, 0f);
                }
                break;
            case 3:
                pressureAmmoLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                pressureAmmoLvl2Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                pressureAmmoLvl3Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                break;
        }

        switch (playerController.oxygenLevel)
        {
            case 0:
                if (inventary.cash < 10000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 10000 && inventary.cash < 30000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 30000 && inventary.cash < 70000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 70000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0f);
                }
                break;
            case 1:
                if (inventary.cash < 30000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 30000 && inventary.cash < 70000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 70000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl2Btn.image.color = new Color(255f, 0f, 0f, 0f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0f);
                }
                break;
            case 2:
                if (inventary.cash < 70000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl2Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0.64f);
                }
                else if (inventary.cash >= 70000)
                {
                    oxygenCapLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl2Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                    oxygenCapLvl3Btn.image.color = new Color(255f, 0f, 0f, 0f);
                }
                break;
            case 3:
                oxygenCapLvl1Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                oxygenCapLvl2Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                oxygenCapLvl3Btn.image.color = new Color(0f, 255f, 0f, 0.64f);
                break;
        }
    }

    public void OnPressureAmmoLvl1()
    {
        if (inventary.cash >= 5000 && playerController.pressureLevel < 1 && isOpen)
        {
            inventary.cash -= 5000;
            playerController.UpgradePressureAmmoLvl(1);
            cashTxt.text = inventary.cash.ToString();
            pressureAmmoLvl1.SetActive(true);
            UpdateView();
        }
    }

    public void OnPressureAmmoLvl2()
    {
        if (inventary.cash >= 20000 && playerController.pressureLevel < 2 && isOpen)
        {
            inventary.cash -= 20000;
            playerController.UpgradePressureAmmoLvl(2);
            cashTxt.text = inventary.cash.ToString();
            pressureAmmoLvl1.SetActive(false);
            pressureAmmoLvl2.SetActive(true);
            UpdateView();
        }
    }

    public void OnPressureAmmoLvl3()
    {
        if (inventary.cash >= 50000 && playerController.pressureLevel < 3 && isOpen)
        {
            inventary.cash -= 50000;
            playerController.UpgradePressureAmmoLvl(3);
            cashTxt.text = inventary.cash.ToString();
            pressureAmmoLvl1.SetActive(false);
            pressureAmmoLvl2.SetActive(false);
            pressureAmmoLvl3.SetActive(true);
            UpdateView();
        }
    }

    public void OnOxygenCapacityLvl1()
    {
        if (inventary.cash >= 10000 && playerController.oxygenLevel < 1 && isOpen)
        {
            inventary.cash -= 10000;
            playerController.UpgradeOxygenCapLvl(1);
            cashTxt.text = inventary.cash.ToString();
            oxygenCapLvl1.SetActive(true);
            UpdateView();
        }
    }

    public void OnOxygenCapacityLvl2()
    {
        if (inventary.cash >= 30000 && playerController.oxygenLevel < 2 && isOpen)
        {
            inventary.cash -= 30000;
            playerController.UpgradeOxygenCapLvl(2);
            cashTxt.text = inventary.cash.ToString();
            oxygenCapLvl1.SetActive(false);
            oxygenCapLvl2.SetActive(true);
            UpdateView();
        }
    }

    public void OnOxygenCapacityLvl3()
    {
        if (inventary.cash >= 70000 && playerController.oxygenLevel < 3 && isOpen)
        {
            inventary.cash -= 70000;
            playerController.UpgradeOxygenCapLvl(3);
            cashTxt.text = inventary.cash.ToString();
            oxygenCapLvl1.SetActive(false);
            oxygenCapLvl2.SetActive(false);
            oxygenCapLvl3.SetActive(true);
            UpdateView();
        }
    }
}
