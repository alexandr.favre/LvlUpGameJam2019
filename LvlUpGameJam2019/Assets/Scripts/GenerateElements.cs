﻿using UnityEngine;

public class GenerateElements : MonoBehaviour
{
    public Transform player;

    public GameObject obj_mine;
    public GameObject obj_diamant;
    public GameObject obj_gold;

    public GameObject fish_green;
    public GameObject fish_blue;
    public GameObject fish_red;
    public GameObject fish_yellow;

    public GameObject fish_r_bee;
    public GameObject fish_r_blue;
    public GameObject fish_r_overkitch;
    public GameObject fish_r_yellow;
    public GameObject fish_r_red;
    public GameObject fish_r_green;

    public GameObject fish_tr_blue;
    public GameObject fish_tr_yellow;
    public GameObject fish_tr_orange;
    public GameObject fish_tr_pink;

    public GameObject fish_final;

    public int nbEasyFish = 50;
    public int nbMediumFish = 30;
    public int nbHardFish = 10;
    public int nbSpecialFish = 5;

    public int nbMine = 25;
    public int nbGold = 10;
    public int nbDiamant = 3;

    public float min_y = -125f;
    public float spawn_zone_width = -50f;
    public float mine_start_y = -30f;
    public float gold_start_y = -40f;
    public float diamant_start_y = -70f;

    private int nbBlueFish;
    private int nbGreenFish;
    private int nbRedFish;
    private int nbYellowFish;

    void Start()
    {
        nbBlueFish = nbEasyFish;
        nbGreenFish = nbEasyFish;
        nbRedFish = nbEasyFish;
        nbYellowFish = nbEasyFish;

        for (int i = 0; i < nbMine; i++)
        {
            GameObject mine =
            Instantiate(obj_mine) as GameObject;
            mine.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(mine_start_y, min_y));
        }

        for (int i = 0; i < nbGold; i++)
        {
            GameObject gold =
            Instantiate(obj_gold) as GameObject;
            gold.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(gold_start_y, min_y));
        }

        for (int i = 0; i < nbDiamant; i++)
        {
            GameObject diamant =
            Instantiate(obj_diamant) as GameObject;
            diamant.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(diamant_start_y, min_y));
        }

        for (int i = 0; i < nbEasyFish; i++)
        {
            GameObject fishGreen =
            Instantiate(fish_green) as GameObject;
            fishGreen.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishGreen.GetComponent<FishMovement>().top_limit, fishGreen.GetComponent<FishMovement>().bottom_limit));
            GameObject fishBlue =
            Instantiate(fish_blue) as GameObject;
            fishBlue.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishBlue.GetComponent<FishMovement>().top_limit, fishBlue.GetComponent<FishMovement>().bottom_limit));
            GameObject fishRed =
            Instantiate(fish_red) as GameObject;
            fishRed.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishRed.GetComponent<FishMovement>().top_limit, fishRed.GetComponent<FishMovement>().bottom_limit));
            GameObject fishYellow =
            Instantiate(fish_yellow) as GameObject;
            fishYellow.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishYellow.GetComponent<FishMovement>().top_limit, fishYellow.GetComponent<FishMovement>().bottom_limit));
        }

        for (int i = 0; i < nbMediumFish; i++)
        {
            GameObject fishRBee =
            Instantiate(fish_r_bee) as GameObject;
            fishRBee.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishRBee.GetComponent<FishMovement>().top_limit, fishRBee.GetComponent<FishMovement>().bottom_limit));
            GameObject fishRBlue =
            Instantiate(fish_r_blue) as GameObject;
            fishRBlue.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishRBlue.GetComponent<FishMovement>().top_limit, fishRBlue.GetComponent<FishMovement>().bottom_limit));
            GameObject fishRYellow =
            Instantiate(fish_r_yellow) as GameObject;
            fishRYellow.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishRYellow.GetComponent<FishMovement>().top_limit, fishRYellow.GetComponent<FishMovement>().bottom_limit));
            GameObject fishRRed =
            Instantiate(fish_r_red) as GameObject;
            fishRRed.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishRRed.GetComponent<FishMovement>().top_limit, fishRRed.GetComponent<FishMovement>().bottom_limit));
            GameObject fishRGreen =
            Instantiate(fish_r_green) as GameObject;
            fishRGreen.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishRGreen.GetComponent<FishMovement>().top_limit, fishRGreen.GetComponent<FishMovement>().bottom_limit));
        }

        for (int i = 0; i < nbHardFish; i++)
        {
            GameObject fishTRBlue =
            Instantiate(fish_tr_blue) as GameObject;
            fishTRBlue.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishTRBlue.GetComponent<FishMovement>().top_limit, fishTRBlue.GetComponent<FishMovement>().bottom_limit));
            GameObject fishTRYellow =
            Instantiate(fish_tr_yellow) as GameObject;
            fishTRYellow.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishTRYellow.GetComponent<FishMovement>().top_limit, fishTRYellow.GetComponent<FishMovement>().bottom_limit));
            GameObject fishTRPink =
            Instantiate(fish_tr_pink) as GameObject;
            fishTRPink.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishTRPink.GetComponent<FishMovement>().top_limit, fishTRPink.GetComponent<FishMovement>().bottom_limit));
        }

        for (int i = 0; i < nbSpecialFish; i++)
        {
            GameObject fishROverkitch =
            Instantiate(fish_r_overkitch) as GameObject;
            fishROverkitch.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishROverkitch.GetComponent<FishMovement>().top_limit, fishROverkitch.GetComponent<FishMovement>().bottom_limit));
            GameObject fishTROrange =
            Instantiate(fish_tr_orange) as GameObject;
            fishTROrange.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishTROrange.GetComponent<FishMovement>().top_limit, fishTROrange.GetComponent<FishMovement>().bottom_limit));

        }

        GameObject fishFinal =
        Instantiate(fish_final) as GameObject;
        fishFinal.transform.position = new Vector2(Random.Range(-spawn_zone_width, spawn_zone_width), Random.Range(fishFinal.GetComponent<FishMovement>().top_limit, fishFinal.GetComponent<FishMovement>().bottom_limit));
    }

    public void CatchGreenFish()
    {
        nbGreenFish--;
        if (nbGreenFish < 5)
        {
            float x;
            if (player.position.x >= 0) x = player.position.x - 40f;
            else x = player.position.x + 40f;
            GameObject fishGreen = Instantiate(fish_green) as GameObject;
            fishGreen.transform.position = new Vector2(x, Random.Range(fishGreen.GetComponent<FishMovement>().top_limit, fishGreen.GetComponent<FishMovement>().bottom_limit));
        }
    }

    public void CatchBlueFish()
    {
        nbBlueFish--;
        if (nbBlueFish < 5)
        {
            float x;
            if (player.position.x >= 0) x = player.position.x - 40f;
            else x = player.position.x + 40f;
            GameObject fishBlue = Instantiate(fish_blue) as GameObject;
            fishBlue.transform.position = new Vector2(x, Random.Range(fishBlue.GetComponent<FishMovement>().top_limit, fishBlue.GetComponent<FishMovement>().bottom_limit));
        }
    }

    public void CatchRedFish()
    {
        nbRedFish--;
        if (nbRedFish < 5)
        {
            float x;
            if (player.position.x >= 0) x = player.position.x - 40f;
            else x = player.position.x + 40f;
            GameObject fishRed = Instantiate(fish_red) as GameObject;
            fishRed.transform.position = new Vector2(x, Random.Range(fishRed.GetComponent<FishMovement>().top_limit, fishRed.GetComponent<FishMovement>().bottom_limit));
        }
    }

    public void CatchYellowFish()
    {
        nbYellowFish--;
        if (nbYellowFish < 5)
        {
            float x;
            if (player.position.x >= 0) x = player.position.x - 40f;
            else x = player.position.x + 40f;
            GameObject fishYellow = Instantiate(fish_yellow) as GameObject;
            fishYellow.transform.position = new Vector2(x, Random.Range(fishYellow.GetComponent<FishMovement>().top_limit, fishYellow.GetComponent<FishMovement>().bottom_limit));
        }
    }

    void Update()
    {

    }
}
