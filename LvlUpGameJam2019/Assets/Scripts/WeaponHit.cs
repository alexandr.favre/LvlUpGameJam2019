﻿using UnityEngine;

public class WeaponHit : MonoBehaviour {

    public Inventary inventary;
    public PlayerController playerController;
    public GenerateElements generateElements;

    void OnCollisionEnter2D(Collision2D col)
    { 
        if (col.gameObject.tag == "BlueFish")
        {
            inventary.nbBlueFish++;
            Destroy(col.gameObject);
            generateElements.CatchBlueFish();
        }
        else if (col.gameObject.tag == "GreenFish")
        {
            inventary.nbGreenFish++;
            Destroy(col.gameObject);
            generateElements.CatchGreenFish();
        }
        else if (col.gameObject.tag == "RedFish")
        {
            inventary.nbRedFish++;
            Destroy(col.gameObject);
            generateElements.CatchRedFish();
        }
        else if (col.gameObject.tag == "YellowFish")
        {
            inventary.nbYellowFish++;
            Destroy(col.gameObject);
            generateElements.CatchYellowFish();
        }
        else if (col.gameObject.tag == "RBeeFish")
        {
            inventary.nbRBeeFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "RBlueFish")
        {
            inventary.nbRBlueFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "ROverkitchFish")
        {
            inventary.nbGreenBall--;
            inventary.nbBlueBall--;
            playerController.DisableGreenLight();
            playerController.DisableBlueLight();
            inventary.nbROverkitchFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "RYellowFish")
        {
            inventary.nbRYellowFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "RRedFish")
        {
            inventary.nbRRedFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "RGreenFish")
        {
            inventary.nbRGreenFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "TRBlueFish")
        {
            inventary.nbTRBlueFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "TRYellowFish")
        {
            inventary.nbTRYellowFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "TROrangeFish")
        {
            inventary.nbYellowBall--;
            inventary.nbRedBall--;
            playerController.DisableRedLight();
            playerController.DisableYellowLight();
            inventary.nbTROrangeFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "TRPinkFish")
        {
            inventary.nbTRPinkFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "FinalFish")
        {
            inventary.nbYellowBall--;
            inventary.nbRedBall--;
            inventary.nbGreenBall--;
            inventary.nbBlueBall--;
            playerController.DisableRedLight();
            playerController.DisableYellowLight();
            playerController.DisableGreenLight();
            playerController.DisableBlueLight();
            inventary.nbFinalFish++;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "Gold")
        {
            inventary.cash += 5000;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "Mine")
        {
            playerController.Die();
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "Diamant")
        {
            inventary.cash += 15000;
            Destroy(col.gameObject);
        }
    }
}
