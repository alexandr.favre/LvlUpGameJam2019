﻿using UnityEngine;
using UnityEngine.UI;

public class Craft : MonoBehaviour {

    public int qty4Ball = 5;

    public Inventary inventary;
    public Canvas craftCanvas;
    public Rigidbody2D rb2dPlayer;

    public Button greenBallBtn;
    public Button blueBallBtn;
    public Button redBallBtn;
    public Button yellowBallBtn;

    private bool isOpen = false;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isOpen = true;
            inventary.isAvailable = false;
            craftCanvas.enabled = true;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezePosition;

            UpdateView();
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel") && isOpen)
        {
            isOpen = false;
            inventary.isAvailable = true;
            craftCanvas.enabled = false;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }

    void UpdateView()
    {
        if (inventary.nbGreenFish >= qty4Ball) greenBallBtn.image.color = new Color(255f, 0f, 0f, 0f);
        else greenBallBtn.image.color = new Color(255f, 0f, 0f, 0.64f);

        if (inventary.nbBlueFish >= qty4Ball) blueBallBtn.image.color = new Color(255f, 0f, 0f, 0f);
        else blueBallBtn.image.color = new Color(255f, 0f, 0f, 0.64f);

        if (inventary.nbRedFish >= qty4Ball) redBallBtn.image.color = new Color(255f, 0f, 0f, 0f);
        else redBallBtn.image.color = new Color(255f, 0f, 0f, 0.64f);

        if (inventary.nbYellowFish >= qty4Ball) yellowBallBtn.image.color = new Color(255f, 0f, 0f, 0f);
        else yellowBallBtn.image.color = new Color(255f, 0f, 0f, 0.64f);
    }

    public void OnGreenBallBtn()
    {
        if (inventary.nbGreenFish >= qty4Ball)
        {
            inventary.nbGreenFish -= qty4Ball;
            inventary.nbGreenBall += 1;
            UpdateView();
        }
    }

    public void OnBlueBallBtn()
    {
        if (inventary.nbBlueFish >= qty4Ball)
        {
            inventary.nbBlueFish -= qty4Ball;
            inventary.nbBlueBall += 1;
            UpdateView();
        }
    }

    public void OnRedBallBtn()
    {
        if (inventary.nbRedFish >= qty4Ball)
        {
            inventary.nbRedFish -= qty4Ball;
            inventary.nbRedBall += 1;
            UpdateView();
        }
    }

    public void OnYellowBallBtn()
    {
        if (inventary.nbYellowFish >= qty4Ball)
        {
            inventary.nbYellowFish -= qty4Ball;
            inventary.nbYellowBall += 1;
            UpdateView();
        }
    }
}
