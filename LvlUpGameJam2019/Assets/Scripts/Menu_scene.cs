﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_scene : MonoBehaviour
{

    public Canvas canvasMenu;
    public Inventary inventary;
    public Rigidbody2D rb2dPlayer;

    public bool isAvailable;

    public bool isOpen = false;

    private void Start()
    {
        isAvailable = true;
    }

    void Update()
    {
        if (isAvailable)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                if (isOpen)
                {
                    Resume();
                }
                else if (inventary.isAvailable)
                {
                    rb2dPlayer.constraints = RigidbodyConstraints2D.FreezePosition;
                    isOpen = true;
                    inventary.isAvailable = false;
                    canvasMenu.enabled = true;
                }
            }
        }
    }

    public void ContinuePlay()
    {
        Resume();
    }

    public void GoToMenu()
    {
        if (isOpen)
        {
            isAvailable = true;
            isOpen = false;
            inventary.isAvailable = true;
            canvasMenu.enabled = false;
            SceneManager.LoadScene("Menu");
        }
    }

    public void Quit()
    {
        if (isOpen)
        {
            Application.Quit();
        }
    }

    private void Resume()
    {
        if (isOpen)
        {
            isAvailable = true;
            rb2dPlayer.constraints = RigidbodyConstraints2D.FreezeRotation;
            isOpen = false;
            inventary.isAvailable = true;
            canvasMenu.enabled = false;
        }
    }
}
